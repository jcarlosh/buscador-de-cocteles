import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Icoctel } from '../interfaces/icoctel';
import { Ifiltro } from '../interfaces/ifiltro';
import { Coctel } from '../modelos/coctel.model';
import * as _ from 'lodash';
import { map } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class CoctelService {

constructor(private http:HttpClient) {

}

getCoctelFiltro(filtro:Ifiltro){
  const urlbase="https://www.thecocktaildb.com/api/json/v1/1/";
  let adicional="";

  if(filtro.buscar==='name'){
    adicional="search.php?s=" + filtro.value;
  }else {

    adicional="filter.php?";
    if(filtro.buscar==="categoria"){
      adicional+="g=";
    }
else if(filtro.buscar==="categoria"){
      adicional+="c=";
    }
    else{
      adicional+="i=";
    }
    adicional+=filtro.value;
  }
  const urlfinal= urlbase+adicional;
    return this.http.get(urlfinal).pipe(
      map((data: any) => {
        console.log(this.parsedatos(data.drinks));
        return this.parsedatos(data.drinks);
      }));
}
private parsedatos(drinks: any){
  // let newlista:any=[];
  // _.forEach((parsedatos:any, c:any)=>{
  //   let coctel= new Coctel(c);
  //   newlista.push(c);
  // })
  // return newlista;

  const cleanDrinks = drinks.map((drink: any) => {
    const arr15 = [...Array(15).keys()];
    const ingredients = arr15.reduce((prev: any, current, index) => {
      if (drink['strIngredient' + (current + 1)] !== null) {
        prev.push(drink['strIngredient' + (current + 1)])
      }
      return prev;
    }, [])

    return {name : drink.strDrink, ingredients: ingredients };
  })

  const cleanDrinks2 = drinks.map((drink: any) => {
    const ingredients = [];
    const maxLength = 15;
    for (let j = 1; j <= maxLength; j++) {
      if (drink['strIngredient' + j]) {
        const ingredient = drink['strIngredient' + j];
        ingredients.push(ingredient);
      } else {
        break;
      }
    }

    return cleanDrinks;
  })



  return cleanDrinks;
  // const cleanDrinks = [];
  // for (let index = 0; index < drinks.length; index++) {
  //   const drink = drinks[index];
  //   const drinkObj: any = {name: drink.strDrink};

  //   const maxLength = 15;
  //   const ingredients = [];
  //   for (let j = 1; j <= maxLength; j++) {
  //     if (drink['strIngredient' + j] !== null) {
  //       const ingredient = drink['strIngredient' + j];
  //       ingredients.push(ingredient);
  //     } else {
  //       break;
  //     }
  //     drinkObj.ingredients = ingredients;
  //   }
  //   cleanDrinks.push(drinkObj);
  // }
  // return cleanDrinks;
}

}


[
  {
    nombre: 'margarita',
    ingredientes: ['ing1', 'ing2', 'ing3']
  },
  {
    nombre: 'psico sour',
    ingredientes: ['ing1', 'ing2', 'ing3', 'ing4']
  }
]
