import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetalleCoctelComponent } from './componentes/list-cocteles/detalle-coctel/detalle-coctel.component';
import { ListCoctelesComponent } from './componentes/list-cocteles/list-cocteles.component';



@NgModule({
  declarations: [
    AppComponent,
    ListCoctelesComponent,
    DetalleCoctelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
