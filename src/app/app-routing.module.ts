import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { compact } from 'lodash';
import { DetalleCoctelComponent } from './componentes/list-cocteles/detalle-coctel/detalle-coctel.component';
import { ListCoctelesComponent } from './componentes/list-cocteles/list-cocteles.component';

const routes: Routes = [
 { path: 'lista', component:ListCoctelesComponent},
 { path: 'detalle/:id', component: DetalleCoctelComponent},
 {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
