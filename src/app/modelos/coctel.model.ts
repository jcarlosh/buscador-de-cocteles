import { Icoctel } from "../interfaces/icoctel";
import * as _ from 'lodash';

export class Coctel implements Icoctel{
  constructor(data:any) {
    _.set(this, 'data', data)
}
  get id():string{
    return _.get(this, 'data.idDrink')
  }
  get name():string{
    return _.get(this, 'data.strDrink')
  }

  get img():string{
    return _.get(this, 'data.strDrinkThumb')
  }
  get glass():string{
    return _.get(this, 'data.strGlass')
  }
  private list(atrri:string){
    let index:number = 1;
    let element=_.get(this,atrri+index);
    let elements:string[]=[];
    while(element){
      elements.push(element);
      index++;
      element=_.get(this,atrri+index)
    }
    return elements;
  }



  get ingredientes():string[]{
    return this.list('strIngredient');
  }
  get numIngred():number{
    return this.ingredientes.length;
  }
  get instruccion():string{
    return _.get(this, 'data.strInstructionsES') ? _.get(this, 'data.strInstructionsES'):
    _.get(this, 'data.strInstructions')
  }
  get measures():string[]{
    return this.list('strMeasure');
  }
  get numMeasures():number{
    return this.measures.length;
  }



}

