
export interface Icoctel {
  id:string,
  name:string,
  img:string,
  glass:string,
  ingredientes:string[],
  numIngred:number,
  instruccion:string,
  measures:string[] ,
  numMeasures:number,
}



