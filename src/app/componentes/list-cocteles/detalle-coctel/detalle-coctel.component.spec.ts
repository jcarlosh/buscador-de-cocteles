import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCoctelComponent } from './detalle-coctel.component';

describe('DetalleCoctelComponent', () => {
  let component: DetalleCoctelComponent;
  let fixture: ComponentFixture<DetalleCoctelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleCoctelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCoctelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
