import { Component, OnInit } from '@angular/core';
import { Ifiltro } from 'src/app/interfaces/ifiltro';
import { CoctelService } from 'src/app/servicios/coctel.service';


@Component({
  selector: 'app-list-cocteles',
  templateUrl: './list-cocteles.component.html',
  styleUrls: ['./list-cocteles.component.css']
})
export class ListCoctelesComponent implements OnInit {

  
  public estadoboton:Boolean;
  public filtro:Ifiltro;


  constructor( private serviciococtel:CoctelService) {
    this.estadoboton=false;
    this.filtro={
      buscar:'name',
      value:''
    }
   }

  ngOnInit(): void {
  }
  mostrarboton(){
    this.estadoboton=!this.estadoboton;
  }
  filtrodatos(){
    console.log(this.filtro);
    this.serviciococtel.getCoctelFiltro(this.filtro).subscribe(c=>{
      console.log(c);
    })
  }

}
